//
//  CustomerAddress.h
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ServerEntity.h"

@class Customer;

@interface CustomerAddress : NSManagedObject <ServerEntity>

@property (nonatomic, retain) NSNumber * addressId;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSNumber * streetNumber;
@property (nonatomic, retain) NSSet *customers;

/**
 *  An address is considered valid when country, street
 *  and street number have been provided.
 */
- (BOOL) isValid;

@end

@interface CustomerAddress (CoreDataGeneratedAccessors)

- (void)addCustomersObject:(Customer *)value;
- (void)removeCustomersObject:(Customer *)value;
- (void)addCustomers:(NSSet *)values;
- (void)removeCustomers:(NSSet *)values;

@end
