//
//  CustomerDetailViewController.m
//  T1-ERP-App
//
//  Created by Hagi on 8/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "CustomerDetailViewController.h"
#import "CustomerAddress.h"

@interface CustomerDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *customerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameAndIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameAndIdDescriptionLabel;

@property (weak, nonatomic) IBOutlet UITextView *addressTextField;
@property (weak, nonatomic) IBOutlet UILabel *validationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *validationImageView;

@property (nonatomic, strong) Customer *myCustomer;

@end

@implementation CustomerDetailViewController

- (id)initWithCustomer:(Customer *)customer {
    self = [super init];
    if (self) {
        self.myCustomer = customer;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.addressTextField setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    self.customerTitleLabel.text = NSLocalizedString(@"Customer.CustomerDetailViewController.customer", nil);
    self.addressTitleLabel.text = NSLocalizedString(@"Customer.CustomerDetailViewController.address", nil);
    
    self.nameAndIdDescriptionLabel.text = NSLocalizedString(@"Customer.CustomerDetailViewController.custNameAndId", nil);
    
    // Dynamic content from customer object
    self.nameAndIdLabel.text = [NSString stringWithFormat:@"%@ (%@)", self.myCustomer.name, self.myCustomer.customerId];
    
    // these attributes are optional in the data model
    NSString *street = self.myCustomer.address.street;
    NSString *streetNumber = self.myCustomer.address.streetNumber.stringValue;
    
    streetNumber = (streetNumber.integerValue == 0)? @"" : streetNumber;
    
    NSString *address = @"";
    
    if (street || streetNumber) {
        if (!street) {
            street = @"";
        }
        address = [address stringByAppendingFormat:@"\n%@ %@", street, streetNumber];
    }
    address = [address stringByAppendingFormat:@"\n%@", self.myCustomer.address.country];
    self.addressTextField.text = address;
    
    // Address validation
    if ([self.myCustomer.address isValid]) {
        self.validationImageView.image = [UIImage imageNamed:@"address_valid"];
        self.validationLabel.text = NSLocalizedString(@"Customer.CustomerDetailViewController.validationOk", nil);
    } else {
        self.validationImageView.image = [UIImage imageNamed:@"address_not_valid"];
        self.validationLabel.text = NSLocalizedString(@"Customer.CustomerDetailViewController.validationNotOk", nil);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
