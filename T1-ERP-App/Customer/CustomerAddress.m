//
//  CustomerAddress.m
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "CustomerAddress.h"
#import "Customer.h"


@implementation CustomerAddress

@dynamic addressId;
@dynamic country;
@dynamic street;
@dynamic streetNumber;
@dynamic customers;

- (BOOL)isValid {
    if ([self.street isEqualToString:@""] || [self.country isEqualToString:@""] || self.streetNumber.integerValue == 00) {
        return NO;
    }
    return YES;
}

#pragma mark - ServerEntity Protocol

- (void)updateWithInformation:(NSDictionary *)infoDict {
    
    BOOL hasChanged = NO;
    
    if (!self.addressId) {
        // initial creation
        self.addressId = [NSNumber numberWithInteger:[[infoDict objectForKey:@"id"] integerValue]];
        DLog(@"New address with ID '%@'", self.addressId);
    }
    
    NSString *newCountry = [infoDict objectForKey:@"country"];
    if (newCountry && ![self.country isEqualToString:newCountry]) {
        self.country = newCountry;
        hasChanged = YES;
    }
    
    NSString *newStreet = [infoDict objectForKey:@"street"];
    if (newStreet && ![self.street isEqualToString:newStreet]) {
        self.street = newStreet;
        hasChanged = YES;
    }
    
    NSString *newStreetNumber = [infoDict objectForKey:@"number"];
    if (newStreetNumber && (newStreetNumber.integerValue) != self.streetNumber.integerValue) {
        self.streetNumber = [NSNumber numberWithInteger:newStreetNumber.integerValue];
        hasChanged = YES;
    }
    
    if (hasChanged) {
        DLog(@"Address '%@' has changed.", self.addressId);
    }
    // never set customers directly
}

@end
