//
//  Customer.h
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ServerEntity.h"

@class CustomerAddress, Order;

@interface Customer : NSManagedObject <ServerEntity>

@property (nonatomic, retain) NSNumber * customerId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) CustomerAddress *address;
@property (nonatomic, retain) NSSet *orders;
@end

@interface Customer (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(Order *)value;
- (void)removeOrdersObject:(Order *)value;
- (void)addOrders:(NSSet *)values;
- (void)removeOrders:(NSSet *)values;

@end
