//
//  CustomerCollectionViewCell.m
//  T1-ERP-App
//
//  Created by Hagi on 8/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "CustomerCollectionViewCell.h"
#import "CustomerAddress.h"

#define SELECTED_BACKGROUND_VIEW_TAG 230859674543

@interface CustomerCollectionViewCell ()

/**
 *  The title lable, filled with the customer's name.
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/**
 *  The image view displays a status image.
 *
 *  Green indicates a valid address. Grey indicates an invalid address.
 */
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

@end

@implementation CustomerCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CustomerCollectionViewCell" owner:self options:nil];
        if ([arrayOfViews count] < 1) {
            return nil;
        }
    
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        self = [arrayOfViews objectAtIndex:0];
        self.layer.cornerRadius = 15;
        
        UIView *selBackgroundView = [self viewWithTag:SELECTED_BACKGROUND_VIEW_TAG];
        [selBackgroundView removeConstraints:selBackgroundView.constraints];
        self.selectedBackgroundView = selBackgroundView;
        selBackgroundView.hidden = NO;
        //[selBackgroundView removeFromSuperview];
    }
    return self;
    
}

- (void)displayCustomer:(Customer *)customer {
    self.titleLabel.text = customer.name;
    
    if ([customer.address isValid]) {
        self.statusImage.image = [UIImage imageNamed:@"address_valid"];
    } else {
        self.statusImage.image = [UIImage imageNamed:@"address_not_valid"];
    }
}

@end
