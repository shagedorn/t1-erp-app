//
//  CustomerDetailViewController.h
//  T1-ERP-App
//
//  Created by Hagi on 8/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customer.h"

/**
 *  To be displayed in a popover view controller.
 */
@interface CustomerDetailViewController : UIViewController

- (id) initWithCustomer:(Customer*) customer;

@end
