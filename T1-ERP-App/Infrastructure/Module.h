//
//  Module.h
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

/**
 *  Forward declaration as Module protocol is used in typedefs
 */
@protocol Module;

/**
 *  Typedefs for blocks used in the protocol
 */
typedef void (^UpdateBlock)(NSNumber *progress, id<Module> sender);
typedef void (^CompletionBlock)(NSError *error, id<Module> sender);
typedef void (^AnimationHasFinishedBlock)();
typedef void (^CloseModuleBlock)(AnimationHasFinishedBlock block);

/**
 *  Every module should implement this protocol in order to be launched
 *  by the launch screen.
 */
@protocol Module <NSObject>

@property (nonatomic, copy) CloseModuleBlock closeModuleBlock;

/**
 *  The image will be displayed on the launch screen.
 *
 *  Ideally, it is 200x200px in size (400x400 for retina displays).
 *  It will not be scaled up, but scaled down if bigger.
 */
- (UIImage*) modulePreviewImage;

/**
 *  The controller will be displayed when the module is launched.
 *
 *  It is expected to provide UI and functionality to launch
 *  all further view controllers of this module.
 */
- (UIViewController*) moduleLaunchController;

/**
 *  A list of modules that have to be loaded
 *  before this module's entities can safely be added
 *  to the database.
 *
 *  Use the same identifier that the depending modules
 *  return from moduleDescription.
 */
- (NSArray*) moduleDependencies;

/**
 *  Perform initial or incremental data loading. When new data has been
 *  inserted, the updateBlock should be executed. When loading data has
 *  finished, the completionBlock should be executed.
 *
 *  The loading should be performed on a separate execution queue.
 */
- (void) moduleLoadData:(NSManagedObjectContext*)context
            usingBlockForProgress:(UpdateBlock)updateBlock
                    andCompletion:(CompletionBlock)completionBlock;

/**
 *  Return a short, descriptive name of the module.
 *
 *  This name is also used for dependency resolution.
 */
- (NSString*) moduleDescription;

@end
