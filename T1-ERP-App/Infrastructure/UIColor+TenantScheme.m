//
//  UIColor+TenantScheme.m
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "UIColor+TenantScheme.h"

@implementation UIColor (TenantScheme)

+ (UIColor *)progressViewTintColour {
    return [UIColor redColor];
}

+ (UIColor *)navigationBarTintColour {
    return [UIColor redColor];
}

@end
