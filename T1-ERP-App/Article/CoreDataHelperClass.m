//
//  CoreDataHelperClass.m
//  T1-ERP-App
//
//  Created by Hagi on 7/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "CoreDataHelperClass.h"

@implementation CoreDataHelperClass

+ (NSManagedObject *)checkForExistingEntity:(NSString *)entityName
                          usingKeyAttribute:(NSString *)attributeName
                                  withValue:(id)attributeValue
                                  inContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];

    // %K does not add quotation marks!
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", attributeName, attributeValue];
    request.predicate = predicate;

    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];

    // error handling
    if (error) {
        DLog(@"Error occurred: %@", error);
        return nil;
    }
    if (results.count > 1) {
        DLog(@"WARNING: More than one %@ object with %@ = %@ exists.", entityName, attributeName, attributeValue);
        return [results lastObject];
    }
    if (results.count == 0 || !results) {
        // no object found
        DLog(@"%@ object with %@ = %@ does not exist.", entityName, attributeName, attributeValue);
        return nil;
    } else {
        return [results lastObject];
    }
}

+ (NSError*)saveOrUpdateAllServerEntitiesOfType:(Class<ServerEntity>)entityClass
                       withPrimaryDictionaryKey:(NSString *)dictKey
                        andPrimaryAttributeName:(NSString *)attributeName
                                     fromSource:(NSArray *)objects
                                           into:(NSManagedObjectContext *)context {

    for (NSDictionary *objectInfo in objects) {
        NSString *primaryKey = [objectInfo objectForKey:dictKey];

        NSManagedObject<ServerEntity> *existingObject = (NSManagedObject<ServerEntity>*)[CoreDataHelperClass
                                                         checkForExistingEntity:NSStringFromClass(entityClass)
                                                         usingKeyAttribute:attributeName
                                                         withValue:[NSNumber numberWithInteger:[primaryKey integerValue]]
                                                         inContext:context];
        if (existingObject) {
            // Object already exists - just update
            [existingObject updateWithInformation:(NSDictionary*)objectInfo];
        } else {
            // Colour has not been loaded before - initialise
            NSManagedObject<ServerEntity> *newObject = (NSManagedObject<ServerEntity>*)[NSEntityDescription
                                        insertNewObjectForEntityForName:NSStringFromClass(entityClass)
                                        inManagedObjectContext:context];
            [newObject updateWithInformation:objectInfo];
        }
    }

    NSError* error = nil;
    [context save:&error];
    return error;
}

@end
