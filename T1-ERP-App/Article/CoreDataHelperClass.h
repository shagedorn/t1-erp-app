//
//  CoreDataHelperClass.h
//  T1-ERP-App
//
//  Created by Hagi on 7/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerEntity.h"

/**
 *  Encapsulates methods that help using core data.
 */
@interface CoreDataHelperClass : NSObject

/**
 *  Checks if an object with the given value of 'attributeName'
 *  already exists in the store.
 *
 *  @return:    The existing object, if found. Else nil.
 */
+ (NSManagedObject*) checkForExistingEntity:(NSString*)entityName
                          usingKeyAttribute:(NSString*)attributeName
                                  withValue:(id)attributeValue
                                  inContext:(NSManagedObjectContext*)context;

/**
 *  All the objects from the source array will be either inserted
 *  into the persistent store or updated, if they existed before.
 *  Whether or not an object already exists is decided by the
 *  primary key defined by attributeName.
 *
 *  Assumes that the primary key can be transformed into an integer value.
 */
+ (NSError*) saveOrUpdateAllServerEntitiesOfType:(Class<ServerEntity>)entityClass
                        withPrimaryDictionaryKey:(NSString*)dictKey
                         andPrimaryAttributeName:(NSString*)attributeName
                                      fromSource:(NSArray*)objects
                                            into:(NSManagedObjectContext*)context;

@end
