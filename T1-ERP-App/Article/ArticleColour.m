//
//  ArticleColour.m
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "ArticleColour.h"
#import "Article.h"


@implementation ArticleColour

#pragma mark - Core Data Properties

@dynamic articleColourId;
@dynamic name;
@dynamic articles;

#pragma mark - ServerEntity Protocol

- (void)updateWithInformation:(NSDictionary *)infoDict {

    BOOL hasChanged = NO;

    // Set ID
    if (!self.articleColourId) {
        // Do not overwrite existing IDs
        NSString *initialId = [infoDict objectForKey:@"id"];
        self.articleColourId = [[NSNumber alloc] initWithInteger:[initialId integerValue]];
        DLog(@"ArticleColour '%@' has been initialised.", self.articleColourId);
    }

    // Set name
    NSString *newName = [infoDict objectForKey:@"name"];
    if (newName && ![newName isEqualToString:self.name]) {
        self.name = newName;
        hasChanged = YES;
    }

    if (hasChanged) {
        DLog(@"ArticleColour '%@' has changed.", self.name);
    }
    
    // articles is never set directly
}

#pragma mark - Custom methods

+ (ArticleColour *)colourWithID:(NSInteger)colourId inContext:(NSManagedObjectContext *)context {

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"articleColourId == %d", colourId];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([ArticleColour class])];
    request.predicate = predicate;

    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];

    if (error) {
        DLog(@"Error: %@", error);
        return nil;
    }
    if (!results || (results.count != 1)) {
        DLog(@"No colour with ID == %d found.", colourId);
        return nil;
    }
    return [results lastObject];
}

@end
