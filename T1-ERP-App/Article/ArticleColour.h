//
//  ArticleColour.h
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ServerEntity.h"

@class Article;

/**
 *  Represents the colour of an article.
 */
@interface ArticleColour : NSManagedObject <ServerEntity>

@property (nonatomic, retain) NSNumber * articleColourId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *articles;

+ (ArticleColour*) colourWithID:(NSInteger)colourId inContext:(NSManagedObjectContext*)context;

@end

/**
 *  Generated Accessors
 */
@interface ArticleColour (CoreDataGeneratedAccessors)

- (void)addArticlesObject:(Article *)value;
- (void)removeArticlesObject:(Article *)value;
- (void)addArticles:(NSSet *)values;
- (void)removeArticles:(NSSet *)values;

@end
