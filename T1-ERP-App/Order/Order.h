//
//  Order.h
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ServerEntity.h"

@class Article, Customer;

@interface Order : NSManagedObject <ServerEntity>

@property (nonatomic, retain) NSNumber * orderId;
@property (nonatomic, retain) NSString * orderDescription;
@property (nonatomic, retain) NSSet *articles;
@property (nonatomic, retain) Customer *customer;

@end

@interface Order (CoreDataGeneratedAccessors)

- (void)addArticlesObject:(Article *)value;
- (void)removeArticlesObject:(Article *)value;
- (void)addArticles:(NSSet *)values;
- (void)removeArticles:(NSSet *)values;

@end
