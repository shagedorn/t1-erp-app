//
//  Order.m
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "Order.h"
#import "Article.h"
#import "Customer.h"

#import "CoreDataHelperClass.h"
#import "CoreDataAppDelegate.h"

@implementation Order

#pragma mark - Core Data Properties

@dynamic orderId;
@dynamic orderDescription;
@dynamic articles;
@dynamic customer;

#pragma mark - ServerEntity Protocol

- (void)updateWithInformation:(NSDictionary *)infoDict {

    BOOL hasChanged = NO;
    
    // Never overwrite the ID once set
    if (!self.orderId) {
        self.orderId = @([[infoDict objectForKey:@"id"] integerValue]);
        DLog(@"Inserted Order with ID '%@'", self.orderId)
    }
    
    // Update description
    NSString *newDescription = [infoDict objectForKey:@"description"];
    if (newDescription && ![newDescription isEqualToString:self.orderDescription]) {
        self.orderDescription = newDescription;
        hasChanged = YES;
    }
    
    // Update articles
    NSArray *newArticleIds = [infoDict objectForKey:@"articles"];
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    if (newArticleIds) {
        NSMutableArray *newArticles = [NSMutableArray arrayWithCapacity:newArticleIds.count];
        for (NSString *articleId in newArticleIds) {
            Article *newArticle = (Article*)[CoreDataHelperClass checkForExistingEntity:@"Article"
                                                            usingKeyAttribute:@"articleId"
                                                                    withValue:@(articleId.integerValue)
                                                                    inContext:[appDelegate managedObjectContext]];
            if (newArticle) {
                [newArticles addObject:newArticle];
            } else {
                DLog(@"WARNING: Article '%d' referenced in order '%@' could not be found in database.", articleId.integerValue, self.orderId);
            }
        }
        int countBefore = self.articles.count;
        [self addArticles:[NSSet setWithArray:newArticles]];
        
        if (countBefore != self.articles.count) {
            // simplified check...
            hasChanged = YES;
        }
    }
    
    // Update customer
    NSString *newCustomerId = [infoDict objectForKey:@"customer"];
    if (newCustomerId) {
        Customer *newCustomer = (Customer*)[CoreDataHelperClass checkForExistingEntity:@"Customer"
                                                          usingKeyAttribute:@"customerId"
                                                                  withValue:@(newCustomerId.integerValue)
                                                                  inContext:[appDelegate managedObjectContext]];
        if ((newCustomer.customerId.integerValue != self.customer.customerId.integerValue) && newCustomer) {
            self.customer = newCustomer;
            hasChanged = YES;
        }
    }
    
    if (hasChanged) {
        DLog(@"Order '%@' has changed.", self.orderDescription);
    }
}

@end
