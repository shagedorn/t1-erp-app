//
//  OrderViewController.m
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderViewController.h"
#import "OrderListViewController.h"
#import "OrderByCustomerViewController.h"
#import "Order.h"

#import "CoreDataAppDelegate.h"
#import "CoreDataHelperClass.h"
#import "UIColor+TenantScheme.h"

#import "AFNetworking.h"

#define REFRESH_BUTTON_TAG 889809896
#define REFRESH_INDICATOR_TAG 321230706

#pragma mark - Private Interface

@interface OrderViewController ()

@property (nonatomic, copy) UpdateBlock updateBlock;
@property (nonatomic, copy) CompletionBlock completionBlock;
@property (nonatomic, strong) NSOperationQueue *orderLoadingQueue;

- (void) close:(id)sender;
- (void) refresh:(id)sender;

- (void)loadingHasFinished:(NSData*)responseData;
- (void)loadingFailedWithError:(NSError*)error;

@end

#pragma mark - Implementation

@implementation OrderViewController {}

#pragma mark - Accessors

- (NSOperationQueue *)orderLoadingQueue {
    if (!_orderLoadingQueue) {
        _orderLoadingQueue = [[NSOperationQueue alloc] init];
    }
    return _orderLoadingQueue;
}

#pragma mark - Module Protocol

@synthesize closeModuleBlock = _closeModuleBlock;

- (UIImage *)modulePreviewImage {
    return [UIImage imageNamed:@"module_preview_order"];
}

- (UIViewController *)moduleLaunchController {
    return self;
}

- (NSArray *)moduleDependencies {
    // articles and customers need to be present before orders can be imported
    return @[@"Article", @"Customer"];
}

- (void)moduleLoadData:(NSManagedObjectContext *)context
 usingBlockForProgress:(UpdateBlock)updateBlock
         andCompletion:(CompletionBlock)completionBlock {

    self.updateBlock = updateBlock;
    self.completionBlock = completionBlock;

    // Prevent another loading process
    ((UIButton*)[self.view viewWithTag:REFRESH_BUTTON_TAG]).enabled = NO;
    
    NSMutableString *serverURL = [[[NSBundle mainBundle].infoDictionary objectForKey:@"ServerURL"] mutableCopy];
    NSString *localPlistPath = [[NSBundle mainBundle] pathForResource:@"OrderServerInfo" ofType:@"plist"];
    NSDictionary *localPlist = [NSDictionary dictionaryWithContentsOfFile:localPlistPath];
    [serverURL appendFormat:@"%@.%@", [localPlist objectForKey:@"filename"], [localPlist objectForKey:@"format"]];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverURL]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    // Update handler
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float ratio = (float)totalBytesRead/(float)totalBytesExpectedToRead;
        if (self.updateBlock) {
            self.updateBlock(@(ratio), self);
        }
    }];
    
    // Completion handler
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (self.completionBlock) {
            self.completionBlock(nil, self);
        }
        [self loadingHasFinished:responseObject];
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         if (self.completionBlock) {
                                             self.completionBlock(error, self);
                                         }
                                         [self loadingFailedWithError:error];
                                     }];
    
    [(UIActivityIndicatorView*)[self.view viewWithTag:REFRESH_INDICATOR_TAG] startAnimating];
    [self.orderLoadingQueue addOperation:operation];
}

- (NSString *)moduleDescription {
    return @"Order";
}

#pragma mark - Loading progress

- (void)loadingHasFinished:(NSData*)responseData {
    ((UIButton*)[self.view viewWithTag:REFRESH_BUTTON_TAG]).enabled = YES;
    [(UIActivityIndicatorView*)[self.view viewWithTag:REFRESH_INDICATOR_TAG] stopAnimating];
    
    // Save orders in Core Data
    NSString *resultAsString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *resultDict = [resultAsString propertyList];
    
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    NSArray *orders = [resultDict objectForKey:@"orders"];
    NSError *error = [CoreDataHelperClass saveOrUpdateAllServerEntitiesOfType:[Order class]
                                    withPrimaryDictionaryKey:@"id"
                                     andPrimaryAttributeName:@"orderId"
                                                  fromSource:orders
                                                        into:[appDelegate managedObjectContext]];
    if (error) {
        DLog(@"Error inserting order objects.");
    }
}

- (void)loadingFailedWithError:(NSError*)error {
    ((UIButton*)[self.view viewWithTag:REFRESH_BUTTON_TAG]).enabled = YES;
    [(UIActivityIndicatorView*)[self.view viewWithTag:REFRESH_INDICATOR_TAG] stopAnimating];
    DLog(@"Error loading Orders: %@", error);
}

#pragma mark - User interaction

- (void) close:(id)sender {
    self.closeModuleBlock(^{
        self.view = nil;
        self.viewControllers = nil;
    });
}

- (void) refresh:(id)sender {
    
    // Other modules may need to be loaded first?
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    for (NSString *moduleDescription in [self moduleDependencies]) {
        // this is a simplified check: Does any instance of the
        // module's main entity exist?
        NSFetchRequest *simpleRequest = [[NSFetchRequest alloc] initWithEntityName:moduleDescription];
        NSArray *results = [[appDelegate managedObjectContext] executeFetchRequest:simpleRequest error:nil];
        
        if (!results || results.count == 0) {
            DLog(@"'%@' entities missing. Cannot start loading.", moduleDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order.OrderViewController.Alert.ErrorTitle", nil)
                                                            message:NSLocalizedString(@"Order.OrderViewController.Alert.DependencyErrorMessage", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Order.OrderViewController.Alert.OK", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
            // no further action
            return;
        }
    }
    
    [self moduleLoadData:[appDelegate managedObjectContext]
   usingBlockForProgress:nil
           andCompletion:nil];
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Load tab controllers
    OrderListViewController *listCtr = [[OrderListViewController alloc] init];
    OrderByCustomerViewController *orderByCustomerCtr = [[OrderByCustomerViewController alloc] init];
    
    [self setViewControllers:@[listCtr, orderByCustomerCtr] animated:NO];
    
    // Allow the user to close the controller
    CGRect barFrame = CGRectMake(-10, 0, 150, 60);
    UINavigationBar *partialBar = [[UINavigationBar alloc] initWithFrame:barFrame];
    partialBar.layer.masksToBounds = YES;
    partialBar.layer.cornerRadius = 10.0;
    partialBar.tintColor = [UIColor navigationBarTintColour];
    
    // Set close button
    UIButton *fakeBarButton = [[UIButton alloc] init];
    [fakeBarButton setImage:[UIImage imageNamed:@"close_button"] forState:UIControlStateNormal];
    [fakeBarButton setImage:[UIImage imageNamed:@"close_button_pressed"] forState:UIControlStateHighlighted];
    CGRect buttonFrame = fakeBarButton.frame;
    buttonFrame.origin = CGPointMake(20, 25);
    buttonFrame.size = CGSizeMake(29, 29);
    fakeBarButton.frame = buttonFrame;
    [fakeBarButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    // Refresh Button
    UIButton *refreshButton = [[UIButton alloc] init];
    [refreshButton setImage:[UIImage imageNamed:@"refresh_button"] forState:UIControlStateNormal];
    buttonFrame.origin = CGPointMake(110, 24);
    refreshButton.frame = buttonFrame;
    refreshButton.tag = REFRESH_BUTTON_TAG;
    [refreshButton addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventTouchUpInside];
    
    // Progress indicator
    UIActivityIndicatorView *progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    progress.tag = REFRESH_INDICATOR_TAG;
    progress.hidesWhenStopped = YES;
    CGRect progressFrame = progress.frame;
    progressFrame.origin = CGPointMake(72, 29);
    progress.frame = progressFrame;
    
    // Overlays for all tab controllers
    [partialBar addSubview:progress];
    [partialBar addSubview:refreshButton];
    [partialBar addSubview:fakeBarButton];
    [self.view addSubview:partialBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.orderLoadingQueue cancelAllOperations];
}

@end
