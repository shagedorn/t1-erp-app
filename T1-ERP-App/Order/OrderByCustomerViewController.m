//
//  OrderByCustomerViewController.m
//  T1-ERP-App
//
//  Created by Hagi on 9/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderByCustomerViewController.h"
#import "OrderByCustomerDetailViewController.h"

#import "Customer.h"

#import "CoreDataAppDelegate.h"

@interface OrderByCustomerViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *resultsController;
@property (nonatomic, strong) UIPopoverController *currentPopover;

@end

@implementation OrderByCustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"OrderListViewController" bundle:nibBundleOrNil];
    return self;
}

- (void)initFetchRequest {
    NSSortDescriptor *sorting = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[[Customer class] description]];
    request.sortDescriptors = @[sorting];

    // Fetch only customers with orders
    NSPredicate *orderPredicate = [NSPredicate predicateWithFormat:@"orders.@count != 0"];
    request.predicate = orderPredicate;

    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    self.resultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                 managedObjectContext:[appDelegate managedObjectContext]
                                                                   sectionNameKeyPath:nil
                                                                            cacheName:nil];
    self.resultsController.delegate = self;
    NSError *error;
    [self.resultsController performFetch:&error];
    if (error) {
        DLog(@"Error fetiching customers with orders: %@", error);
    }
}

#pragma mark - Tab Bar Appearance

- (NSString *)title {
    return NSLocalizedString(@"Order.OrderByCustomerViewController.Title", nil);
}

- (UITabBarItem *)tabBarItem {
    return [[UITabBarItem alloc] initWithTitle:[self title] image:[UIImage imageNamed:@"tab_contacts"] tag:1];
}

#pragma mark - Table View Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"orderListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    Customer *cellCustomer = [self.resultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = cellCustomer.name;

    NSString *idString = NSLocalizedString(@"Customer.ID", nil);
    NSString *ordersCountString = NSLocalizedString(@"Order.OrderByCustomerViewController.NumberOfOrders", nil);
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@: %@ | %@: %d", idString, cellCustomer.customerId, ordersCountString, cellCustomer.orders.count];

    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // open popover
    if (self.currentPopover) {
        [self.currentPopover dismissPopoverAnimated:YES];
    }

    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    targetRect.origin.x += (self.tableView.frame.size.width/2);

    Customer *selectedCustomer = [self.resultsController objectAtIndexPath:indexPath];
    OrderByCustomerDetailViewController *detailCtr = [[OrderByCustomerDetailViewController alloc] initWithCustomer:selectedCustomer];
    self.currentPopover = [[UIPopoverController alloc] initWithContentViewController:detailCtr];
    [self.currentPopover setPopoverContentSize:CGSizeMake(300, 250)];
    [self.currentPopover presentPopoverFromRect:targetRect
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionLeft
                                       animated:YES];
}

@end
