//
//  OrderListDetailViewController.m
//  T1-ERP-App
//
//  Created by Hagi on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderListDetailViewController.h"

#import "Customer.h"

@interface OrderListDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *textField;

@property (strong, nonatomic) Order *myOrder;

@end

@implementation OrderListDetailViewController

- (id)initWithOrder:(Order *)order {
    if (self = [super init]) {
        self.myOrder = order;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.titleLabel.text = [NSString stringWithFormat:@"%@ (%@)", self.myOrder.orderDescription, self.myOrder.orderId];
    NSString *noOfArticles = NSLocalizedString(@"Order.OrderListDetailViewController.NumberOfArticles", nil);
    NSMutableString *mainText = [NSMutableString stringWithFormat:@"%@: %d", noOfArticles, self.myOrder.articles.count];
    NSString *customerString = NSLocalizedString(@"Customer.CustomerDetailViewController.customer", nil);
    [mainText appendFormat:@"\n\n%@: %@", customerString, self.myOrder.customer.name];

    self.textField.text = mainText;
}

@end
