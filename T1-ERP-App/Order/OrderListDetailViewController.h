//
//  OrderListDetailViewController.h
//  T1-ERP-App
//
//  Created by Hagi on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"

@interface OrderListDetailViewController : UIViewController

- (id) initWithOrder:(Order*)order;

@end
