//
//  OrderViewController.h
//  T1-ERP-App
//
//  Created by Hagi on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Module.h"

@interface OrderViewController : UITabBarController <Module>

@end
