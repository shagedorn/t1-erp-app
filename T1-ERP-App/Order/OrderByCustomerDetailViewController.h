//
//  OrderByCustomerDetailViewController.h
//  T1-ERP-App
//
//  Created by Hagi on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderListDetailViewController.h"

#import "Customer.h"

@interface OrderByCustomerDetailViewController : OrderListDetailViewController

- (id) initWithCustomer:(Customer*)customer;

@end
