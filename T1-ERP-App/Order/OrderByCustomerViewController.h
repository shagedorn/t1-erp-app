//
//  OrderByCustomerViewController.h
//  T1-ERP-App
//
//  Created by Hagi on 9/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListViewController.h"

@interface OrderByCustomerViewController : OrderListViewController

@end
