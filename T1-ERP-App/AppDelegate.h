//
//  AppDelegate.h
//  T1-ERP-App
//
//  Created by Hagi on 5/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataAppDelegate.h"

@interface AppDelegate : UIResponder <CoreDataAppDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;

@end
